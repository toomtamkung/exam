// src/services/AuthService.js

import axios from 'axios';

const url = 'http://localhost:8000/api/';

export default {
  login(credentials) {
    return axios
      .post(url + 'login/', credentials)
      .then(response => response.data);
  },
  signUp(credentials) {
    return axios
      .post(url + 'register/', credentials, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    })
      .then(response => response.data);
  },
  profile() {
    return axios
      .get(url + 'users/me')
      .then(response => response.data);
  },
  update(credentials) {
    return axios
      .post(url + 'users/me', credentials, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    })
      .then(response => response.data);
  },
  changepassword(credentials) {
    return axios
      .post(url + 'users/changepassword', credentials)
      .then(response => response.data);
  },
  logout() {
    return axios
      .get(url + 'users/logout/')
      .then(response => response.data);
  },
  getSecretContent() {
    return axios.get(url + 'secret-route/').then(response => response.data);
  }
};