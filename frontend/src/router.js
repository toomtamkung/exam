import { createRouter, createWebHistory } from 'vue-router'
//import VueRouter from 'vue-router'
// Pages
import Home from './components/HomeComponent'
import Register from './components/RegisterComponent'
import Login from './components/LoginComponent'
import Me from './components/MeComponent'
import Edit from './components/ProfileComponent'
import Password from './components/PasswordComponent'
// Routes
const routes = [
  {
    path: '/',
    name: 'index',
    component: Login,
    meta: {
      requiresAuth: true 
    }
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta: {
      requiresAuth: false
    }
  },
  // USER ROUTES
  {
    path: '/me',
    name: 'me',
    component: Me,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/edit',
    name: 'edit',
    component: Edit,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/password',
    name: 'password',
    component: Password,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/logout',
    name: 'logout',
    meta: {
      requiresAuth: true
    }
  },
]
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
export default router