/*import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#app')
*/
import 'es6-promise/auto'
import axios from 'axios'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import VueAxios from 'vue-axios'
import Index from "./components/IndexComponent.vue";
import router from './router'
import { createApp } from 'vue';
import store from './store';
import './css/login.scss'

// set auth header
axios.defaults.headers.common['Authorization'] = `Bearer ${store.state.token}`;


const app = createApp(Index)

app.use(store)
//app.router = router
app.use(router)

app.use(VueAxios, axios)

app.mount('#app')