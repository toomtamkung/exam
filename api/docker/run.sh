#!/bin/sh

cd /var/www
composer install

php artisan migrate
php artisan cache:clear
php artisan route:cache
php artisan storage:link


/usr/bin/supervisord -c /etc/supervisord.conf