<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Rules\PasswordRule;

class RegisterController extends BaseController
{
    
    public function register(Request $request)
    {
        $messages = [
            'username.required' => 'กรุณากรอกรหัสผ่าน',     
            'username.unique' => 'ไม่สามารถใช้ username นี้ได้เนื่องจากมีอยู่แล้ว',
            'username.regex' => 'username ใช้ได้เฉพาะ A-Z, a-z, 0-9, _',
            'username.max' => 'username ต้องไม่เกิน 12 ตัว',
            'password.min' => 'รหัสผ่านต้องไม่น้อยกว่า 6 ตัว',
            'c_password.same' => 'รหัสผ่านไม่ตรงกัน',
            'password.requied' => 'กรุณากรอกรหัสผ่าน',
            'c_password.requied' => 'กรุณากรอกยืนยันรหัสผ่าน',
            'firstname.required' => 'กรุณากรอกชื่อ',
            'lastname.required' => 'กรุณากรอกนามสกุล',
            'profile_pic.mimes' => 'อัพโหลดรูปภาพได้เฉพาะไฟล์ jpg png'
        ];
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users,username|max:12|regex:/^[\w-]*$/',
            'password' => ['required','min:6',new PasswordRule()],
            'c_password' => 'required|same:password',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'profile_pic' => 'required|mimes:jpg,png|max:3000'
        ],$messages);
        $input = $request->all();
        if($request->file('profile_pic')){
            $path = $request->file('profile_pic')->store('avatars',['disk' => 'public']);
            $input['profile_pic'] = $path;
        }
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        
        $input['password'] = Hash::make($input['password']);
        
        $input['old_password'] = $input['password'];
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->plainTextToken;
        $success['name'] =  $user->getFullname();
   
        return $this->sendResponse($success, 'User register successfully.');
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->plainTextToken; 
            $success['name'] =  $user->getFullname();
   
            return $this->sendResponse($success, 'User login successfully.');
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Username หรือ รหัสผ่านไม่ถูกต้อง']);
        } 
    }
}
