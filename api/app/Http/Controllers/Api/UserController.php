<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Validator;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Rules\PasswordRule;
use Illuminate\Support\Facades\Storage;

class UserController extends BaseController
{
    public function index()
    {
        $users = User::all();
        return response()->json(
            [
                'status' => 'success',
                'users' => $users->toArray()
            ], 200);
    }
    public function show(Request $request, $id)
    {
        $user = User::where('username',$id)->get();
        return response()->json(
            [
                'status' => 'success',
                'id'=>Auth::user()->getId(),
                'user' => $user->toArray()
            ], 200);
    }

    public function me(Request $request)
    {
        $user = User::find(Auth::user()->getId());
        if(is_null($user->profile_pic))
            $user->profile_pic = env('APP_URL').Storage::url('img/avatar.jpg');
        else
            $user->profile_pic = env('APP_URL').Storage::url($user->profile_pic);
        return response()->json(
            [
                'status' => 'success',
                'user' => $user->toArray()
            ], 200);
    }

    public function update(Request $request)
    {
        $messages = [
            'firstname.required' => 'กรุณากรอกชื่อ',
            'lastname.required' => 'กรุณากรอกนามสกุล',
            'profile_pic.mimes' => 'อัพโหลดรูปภาพได้เฉพาะไฟล์ jpg png'
        ];
        $user = User::find(Auth::user()->getId());
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255'
        ],$messages);
        //$user = User::find(Auth::user()->getId());
        $input = $request->all();
        if($request->file('profile_pic')){
            $path = $request->file('profile_pic')->store('avatars',['disk' => 'public']);
            $input['profile_pic'] = $path;
        }
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $user->update($input);
        
        $user->profile_pic = env('APP_URL').Storage::url($user->profile_pic);
        return response()->json(
        [
            'status' => 'success',
            'user' => $user->toArray()
        ], 200);
    }

    public function changepassword(Request $request)
    {
        $messages = [
            'password.min' => 'รหัสผ่านต้องไม่น้อยกว่า 6 ตัว',
            'c_password.same' => 'รหัสผ่านไม่ตรงกัน',
            'password.requied' => 'กรุณากรอกรหัสผ่าน'
        ];
        $user = User::find(Auth::user()->getId());
        $validator = Validator::make($request->all(), [
            'password' => ['required','Min:6', new PasswordRule()],
            'c_password' => 'required|same:password',
        ],$messages);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $input = $request->all();
        $new_password = $input['password'];
        
        $old_passwords = explode(' ',$user->old_password);
        foreach ($old_passwords as $key => $value) {
            if(Hash::check($new_password, $value))
                return $this->sendError('Validation Error.', ['password'=>'การแก้ไข password ห้ามซ้ำใน 5 ครั้งล่าสุดที่เคยแก้ไข password']);
        }
        $input['password'] = Hash::make($input['password']);
        if(sizeof($old_passwords) == 5)
            array_shift($old_passwords);
        array_push($old_passwords, $input['password']);
        $input['old_password'] = implode(' ',$old_passwords);

        $user->update($input);

        return response()->json(
        [
            'status' => 'success',
            'user' => $user->toArray()
        ], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
}