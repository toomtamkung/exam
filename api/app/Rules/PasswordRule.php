<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PasswordRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
            // Check for sequential numerical & alphabetical characters
            $sequence = "0123456789abcdefghijklmnopqrstuvwxyz";
            $length = 6;
            $sequence .= substr($sequence, 0, $length - 1);
            for ($i = 0; $i < strlen($sequence) - $length; $i++) {
                if (strpos(strtolower($value), substr($sequence, $i, $length)) !== false) {
                    return false;
                }
            }
            return true;
        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'รหัสผ่านต้องไม่เป็นตัวอักษรหรือตัวเลขเรียงกัน';
    }
}
