<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);

/*Route::middleware(['auth:sanctum'])->group(function () {
    Route::resource('profile', UserController::class);
});
*/

Route::group(['middleware' => 'auth:sanctum'], function(){
    // Users
    Route::get('users', 'App\Http\Controllers\Api\UserController@index');
    //Route::get('users/{id}', 'App\Http\Controllers\Api\UserController@show');
    Route::get('users/me', 'App\Http\Controllers\Api\UserController@me');
    Route::post('users/me', 'App\Http\Controllers\Api\UserController@update');
    Route::get('users/logout', 'App\Http\Controllers\Api\UserController@logout');
    Route::post('users/changepassword', 'App\Http\Controllers\Api\UserController@changepassword');
});